#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QMediaPlayer>
//#include <unistd.h>
#include <iostream>
#include "mainwindow.h"

#include "figurine.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog {
    Q_OBJECT

   private:
    Ui::Dialog *ui;
    QMediaPlayer *bombSound;
    QMediaPlayer *swordSound;
    QMediaPlayer *victorySound;
    QMediaPlayer *defeatSound;
    QMediaPlayer *deactivateSound;
    QObject *mainWindow;

   public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    static void showBattle(Figurine *figLeft, Figurine *figRight);

    void initializeMusic();
    void playBattleSound(Figurine *figLeft, Figurine *figRight);

   private slots:
    void on_pbOkPressed();
};

#endif  // DIALOG_H
