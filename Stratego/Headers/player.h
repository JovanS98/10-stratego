#ifndef PLAYER_H
#define PLAYER_H

#include <cmath>
#include <vector>
#include "board.h"
#include "field.h"

typedef std::vector<std::vector<Field>> State;

class Player {
   private:
    PlayerType m_playerType;
    Board *m_playerTable;
    Figurine *m_currentSelectedFigurine;

   public:
    Player(PlayerType p);
    ~Player();
    PlayerType getPlayerType();
    Board *getPlayerTable();
    Field *getPlayerField(int i, int j);
    Figurine *getCurrentSelectedFigurine() const;

    void setPlayerType(const PlayerType &playerType);
    void setPlayerTable(Board *playerTable);
    void setField(int x, int y, FieldType fd, Figurine *figurine);
    void setCurrentSelectedFigurine(Figurine *currentSelectedFigurine);

    std::pair<int, bool> evaluateTable(State playerFields);
};

#endif  // PLAYER_H
