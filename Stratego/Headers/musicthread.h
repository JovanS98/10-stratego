﻿#ifndef MUSICTHREAD_H
#define MUSICTHREAD_H

#include <QMediaPlayer>
#include <QString>
#include <QThread>
#include <iostream>
#include "enums.h"

class MusicThread : public QThread {
    Q_OBJECT
   public:
    MusicThread(QObject *parent   = nullptr,
                GameDifficulty gd = GameDifficulty::MEDIUM);

    void setMainWindow(QObject *value);

   protected:
    // void run() override;

   private:
    GameDifficulty selectedDifficulty;
    QMediaPlayer *gameSong;
    QObject *m_parent;
    QObject *mainWindow;
    bool musicPlaying;

   private:
    QString selectedMusicUrl();

   public slots:
    void muteMusic();
    void unmuteMusic();
    void stopMusic();
    void startMusic();
};

#endif  // MUSICTHREAD_H
