#ifndef GAME_H
#define GAME_H

#include "board.h"
#include "gamescene.h"
#include "player.h"

#include <stdlib.h>
#include <time.h>
#include <QGraphicsScene>
#include <QLabel>
#include <QMessageBox>
#include <iostream>
#include <vector>

class MainWindow;
class GameScene;

struct minMaxParams {
    int evaluationScore;
    int oldI, oldJ;
    int newI, newJ;
};

struct stateParams {
    State state;
    int oldI, oldJ;
    int newI, newJ;
};

class Game {
   private:
    Player *m_player1, *m_player2;
    Board *m_board;
    GameScene *m_gameScene;
    Turn m_turn;
    QLabel *m_msg;
    QLabel *m_notification;
    int m_difficulty;
    bool m_gameEnded;

   public:
    Game(Player *player1, Player *player2, Board *board, GameScene *gameScene,
         QLabel *msg);
    Game();
    ~Game();

    std::vector<stateParams> getNextStates(State &, Turn);
    std::tuple<int, int, int, int> findDifference(State oldState,
                                                  State newState);

    int evaluateBoard(State playingBoard);

    Player *getPlayer2() const;
    void setPlayer2(Player *player2);
    Player *getPlayer1() const;
    void setPlayer1(Player *player1);
    Board *getBoard();
    Field *getBoard(int i, int j);
    void setBoard(Board *board);
    void setBoard(Field *f, int i, int j);
    GameScene *getGameScene();
    void setGameScene(GameScene *);
    void drawTable(Board *board, bool firstTime);
    Turn getTurn();
    void setTurn(Turn);

    void playGame();
    minMaxParams maxi(int depth, State &state, int alpha, int beta,
                      bool shouldRandomize);
    minMaxParams mini(int depth, State &state, int alpha, int beta,
                      bool shouldRandomize);
    void copyState(State &st, State &state);
    void resetStateParams(stateParams &stateP, State st);
    void initializeStateParams(stateParams &sp, State st, int oldI, int oldJ,
                               int newI, int newJ);
    int getDifficulty() const;
    void setDifficulty(int difficulty);
    void checkEvaluation(int);
    bool getGameEnded() const;
    QLabel *getNotification() const;
    void setNotification(QLabel *notification);
    bool checkNoMoreMoves(PlayerType playerType, Board *board);

   signals:
    void endGame();
};

#endif  // GAME_H
