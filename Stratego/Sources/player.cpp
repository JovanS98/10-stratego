#include "../Headers/player.h"
#include <iostream>

#define fieldsCollumn 10

Board *Player::getPlayerTable() { return m_playerTable; }

Field *Player::getPlayerField(int i, int j) {
    return (*m_playerTable->getPboard())[i][j];
}

void Player::setPlayerTable(Board *playerTable) { m_playerTable = playerTable; }

PlayerType Player::getPlayerType() { return m_playerType; }

void Player::setPlayerType(const PlayerType &playerType) {
    m_playerType = playerType;
}

void Player::setField(int x, int y, FieldType fd, Figurine *figurine) {
    (*m_playerTable->getPboard())[x][y]->setFieldType(fd);
    (*m_playerTable->getPboard())[x][y]->setFigurine(figurine);
}

Figurine *Player::getCurrentSelectedFigurine() const {
    return m_currentSelectedFigurine;
}

void Player::setCurrentSelectedFigurine(Figurine *currentSelectedFigurine) {
    m_currentSelectedFigurine = currentSelectedFigurine;
}

std::pair<int, bool> Player::evaluateTable(State playerFields) {
    bool hasFlag         = false;
    int evaluation_score = 0;

    for (unsigned i = 0; i < 10; i++) {
        for (unsigned j = 0; j < 10; j++) {
            Figurine *currentFigurine = playerFields[i][j].getFigurine();
            if (currentFigurine == nullptr) {
                continue;
            }
            if (currentFigurine->getOwner() == getPlayerType()) {
                if (currentFigurine->getFigurineType() == FigurineType::Flag) {
                    hasFlag = true;
                    evaluation_score += pow(2, 11);
                }
                int currentStrength =
                    currentFigurine->getFigurineType() == FigurineType::Bomb
                        ? 1
                        : currentFigurine->getStrength();
                evaluation_score += pow(2, currentStrength);
            }
        }
    }
    return std::make_pair(evaluation_score, hasFlag);
}

Player::Player(PlayerType p) {
    m_currentSelectedFigurine = nullptr;

    m_playerType  = p;
    m_playerTable = nullptr;
}

Player::~Player() {
    //    delete m_playerTable;
    //    delete m_currentSelectedFigurine;
}
