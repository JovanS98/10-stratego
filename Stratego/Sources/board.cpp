#include "../Headers/board.h"
#include <iostream>

State &Board::getBoard() { return m_board; }

void Board::setBoard(const State &board) { m_board = board; }

pState Board::getPboard() const { return m_pboard; }

void Board::setPboard(const pState &pboard) { m_pboard = pboard; }

void Board::fromPointerToState() {
    m_board.resize(10);
    for (int i = 0; i < 10; i++) m_board[i].resize(10, Field(FieldType::Free));

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            Field *f = ((*m_pboard)[i][j]);
            m_board[i][j].setFigurine(f->getFigurine());
            m_board[i][j].setXPos(f->getXPos());
            m_board[i][j].setYPos(f->getYPos());
            if (f->getFigurine()) {
                m_board[i][j].getFig().setOldI(f->getFigurine()->getOldI());
                m_board[i][j].getFig().setOldJ(f->getFigurine()->getOldJ());
            }
        }
    }
}

void Board::fromStateToPointer() {
    m_pboard->resize(10);

    for (int i = 0; i < 10; i++) {
        (*m_pboard)[i].resize(10);
    }

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            (*m_pboard)[i][j] = new Field(m_board[i][j]);
        }
    }
}

void Board::setField(int i, int j, State state) {
    m_board[i][j] = state[i][j];
    this->fromStateToPointer();
}

void Board::setField(int i, int j, pState pstate) {
    (*m_pboard)[i][j] = (*pstate)[i][j];
    this->fromPointerToState();
}

bool Board::checkPlayerSetup() {
    firstRowFree        = true;
    allFigurinesSet     = true;
    bombsNotBlockingWay = false;

    for (int i = 0; i < 10; i++) {
        if (i == 2 || i == 3 || i == 6 || i == 7) continue;
        if ((*m_pboard)[5][i]->getFigurine()) {
            firstRowFree = false;
            break;
        }
    }

    bool fastBreak = false;
    for (int i = 6; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            if (!(*m_pboard)[i][j]->getFigurine()) {
                allFigurinesSet = false;
                fastBreak       = true;
                break;
            }
        }
        if (fastBreak) break;
    }

    for (int i = 0; i < 10; i++) {
        if (i == 2 || i == 3 || i == 6 || i == 7) continue;
        Figurine *f = (*m_pboard)[6][i]->getFigurine();
        if (f && f->getFigurineType() != FigurineType::Bomb)
            bombsNotBlockingWay = true;
    }

    if (firstRowFree && allFigurinesSet && bombsNotBlockingWay)
        return true;
    else
        return false;
}

bool Board::getFirstRowFree() const { return firstRowFree; }

bool Board::getAllFigurinesSet() const { return allFigurinesSet; }

bool Board::getBombsNotBlockingWay() const { return bombsNotBlockingWay; }

Board::Board() {
    m_pboard = new std::vector<std::vector<Field *>>();
    m_board.resize(10);
    for (int i = 0; i < 10; i++) {
        m_board[i].resize(10, Field(FieldType::Free));
    }

    m_board[4][2].setFieldType(FieldType::Forbidden);
    m_board[4][2].setSpecialFieldFlag(1);

    m_board[4][6].setFieldType(FieldType::Forbidden);
    m_board[4][6].setSpecialFieldFlag(5);

    m_board[4][3].setFieldType(FieldType::Forbidden);
    m_board[4][3].setSpecialFieldFlag(2);

    m_board[4][7].setFieldType(FieldType::Forbidden);
    m_board[4][7].setSpecialFieldFlag(6);

    m_board[5][2].setFieldType(FieldType::Forbidden);
    m_board[5][2].setSpecialFieldFlag(3);

    m_board[5][6].setFieldType(FieldType::Forbidden);
    m_board[5][6].setSpecialFieldFlag(7);

    m_board[5][3].setFieldType(FieldType::Forbidden);
    m_board[5][3].setSpecialFieldFlag(4);

    m_board[5][7].setFieldType(FieldType::Forbidden);
    m_board[5][7].setSpecialFieldFlag(8);

    this->fromStateToPointer();
}

Board::Board(Board &other) {
    State newBoard = other.getBoard();

    m_board.resize(10);
    for (int i = 0; i < 10; i++) m_board[i].resize(10, Field(FieldType::Free));

    for (int i = 0; i < 10; i++)
        for (int j = 0; j < 10; j++) m_board[i][j] = newBoard[i][j];

    this->fromStateToPointer();
}

Board::Board(State newBoard) {
    m_board.resize(10);
    for (int i = 0; i < 10; i++) m_board[i].resize(10, Field(FieldType::Free));

    for (int i = 0; i < 10; i++)
        for (int j = 0; j < 10; j++) m_board[i][j] = newBoard[i][j];

    this->fromStateToPointer();
}
