#include "../Headers/musicthread.h"

MusicThread::MusicThread(QObject *parent, GameDifficulty gd) : QThread(parent) {
    musicPlaying       = false;
    m_parent           = parent;
    selectedDifficulty = gd;
    gameSong           = new QMediaPlayer();
}

// void MusicThread::run()
//{
//    gameSong = new QMediaPlayer();
//    gameSong->setMedia(QUrl(selectedMusicUrl()));
//    gameSong->play();
//}

void MusicThread::setMainWindow(QObject *value) { mainWindow = value; }

QString MusicThread::selectedMusicUrl() {
    if (selectedDifficulty == GameDifficulty::EASY) {
        return QString("qrc:/sounds/Sounds/easy_play_song.mp3");
    } else if (selectedDifficulty == GameDifficulty::MEDIUM) {
        return QString("qrc:/sounds/Sounds/medium_play_song.mp3");
    } else {
        return QString("qrc:/sounds/Sounds/hard_play_song.mp3");
    }
}

void MusicThread::muteMusic() { gameSong->setMuted(true); }

void MusicThread::unmuteMusic() { gameSong->setMuted(false); }

void MusicThread::stopMusic() {
    if (musicPlaying) {
        gameSong->stop();
        musicPlaying = false;
    }
}

void MusicThread::startMusic() {
    if (!musicPlaying) {
        musicPlaying = true;
        gameSong->setMedia(QUrl(selectedMusicUrl()));
        gameSong->play();
    }
}
