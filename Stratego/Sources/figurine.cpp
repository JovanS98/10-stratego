#include "../Headers/figurine.h"

#include <QGraphicsScene>
#include <QPen>
#include <QPixmap>
#include <QPointF>
#include <QString>
#include "../Headers/board.h"

/***CONSTRUCTORS***/

Figurine::~Figurine() {}

Figurine::Figurine(int x, int y, PlayerType owner, FigurineType figurineType) {
    setXPos(x);
    setYPos(y);
    setWidth(120);
    setHeight(120);
    setStrength(strengthFromFigurineType(figurineType));
    setOwner(owner);
    setFigurineType(figurineType);
    setSelected(false);
    setOldI(-1);
    setOldJ(-1);
    setCrossed(false);
    setInBattle(InBattle::notInBattle);
}

Figurine::Figurine(PlayerType owner, FigurineType figurineType) {
    setXPos(0);
    setYPos(0);
    setWidth(0);
    setHeight(0);
    setStrength(strengthFromFigurineType(figurineType));
    setOwner(owner);
    setFigurineType(figurineType);
    setSelected(false);
    setOldI(-1);
    setOldJ(-1);
    setInBattle(InBattle::notInBattle);
    setCrossed(false);
}

Figurine::Figurine(PlayerType owner, int strength) {
    setXPos(0);
    setYPos(0);
    setWidth(0);
    setHeight(0);
    setStrength(strength);
    setOwner(owner);
    setFigurineType(figurineTypeFromStrength(strength));
    setSelected(false);
    setOldI(-1);
    setOldJ(-1);
    setInBattle(InBattle::notInBattle);
    setCrossed(false);
}

Figurine::Figurine(const Figurine& other) : QGraphicsItem() {
    setXPos(other.getXPos());
    setYPos(other.getYPos());
    setWidth(120);
    setHeight(120);
    setStrength(other.getStrength());
    setOwner(other.getOwner());
    setFigurineType(other.getFigurineType());
    setOldI(other.getOldI());
    setOldJ(other.getOldJ());
    setInBattle(InBattle::notInBattle);
    setCrossed(false);
}

Figurine::Figurine() {
    setXPos(0);
    setYPos(0);
    setWidth(0);
    setHeight(0);
    setStrength(strengthFromFigurineType(FigurineType::None));
    setOwner(PlayerType::Human);
    setFigurineType(FigurineType::None);
    setSelected(false);
    setOldI(-1);
    setOldJ(-1);
    setInBattle(InBattle::notInBattle);
    setCrossed(false);
}

Figurine Figurine::operator=(Figurine other) { return Figurine(other); }

/***METHODS FROM QGRAPHICSITEM***/
QRectF Figurine::boundingRect() const {
    QPointF pointTopLeft = QPointF(getXPos(), getYPos());
    QPointF pointBottomRight =
        QPointF(getXPos() + getWidth(), getYPos() + getHeight());
    return QRectF(pointTopLeft, pointBottomRight);
}

void Figurine::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                     QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    int padding = 5;

    QPixmap qpm;
    if (getOwner() == PlayerType::Computer) {
        if (getInBattle() == InBattle::notInBattle)
            qpm = QPixmap(":/Images/backOfTheFigurine.png");
    } else {
        qpm = QPixmap(
            getImageSource(getOwner(), getFigurineType(), getStrength()));
    }

    if (getInBattle() == InBattle::attack)
        qpm = QPixmap(":/Images/sword.jpeg");

    else if (getInBattle() == InBattle::defend)
        qpm = QPixmap(":/Images/shield.jpeg");

    painter->drawPixmap(getXPos() + padding, getYPos() + padding,
                        getWidth() - 2 * padding, getHeight() - 2 * padding,
                        qpm);

    if (getIsSelected()) {
        QRectF q = boundingRect();

        QPen pen;
        pen.setColor((Qt::green));
        pen.setWidth(2);

        painter->setPen(pen);
        painter->drawRect(q);
    }
}

/***OPERATORS***/
bool Figurine::operator>(const Figurine& other) const {
    /*
     * MOST OF THE TIMES STRENGTH INDICATES IF OUR
     * FIGURINE IS STRONGER THAN THE OTHER.
     * SPECIAL CASES: BOMB VS MINER,
     *                SPY VS MARSHAL
     *
     * */
    if (getFigurineType() == FigurineType::Miner &&
        other.getFigurineType() == FigurineType::Bomb) {
        return true;
    } else if (getFigurineType() == FigurineType::Spy &&
               other.getFigurineType() == FigurineType::Marshal) {
        return true;
    }

    else {
        return getStrength() > other.getStrength();
    }
}

bool Figurine::operator==(const Figurine& other) const {
    if (getFigurineType() != FigurineType::Miner &&
        other.getFigurineType() == FigurineType::Bomb) {
        return true;
    } else if (getFigurineType() == FigurineType::Miner &&
               other.getFigurineType() == FigurineType::Bomb) {
        return false;
    }

    return getStrength() == other.getStrength();
}

bool Figurine::operator<(const Figurine& other) const {
    return other > (*this);
}

/***AUXILIARY METHODS***/
int Figurine::strengthFromFigurineType(FigurineType ft) {
    switch (ft) {
        case FigurineType::Bomb: return 11; break;
        case FigurineType::Marshal: return 10; break;
        case FigurineType::General: return 9; break;
        case FigurineType::Colonel: return 8; break;
        case FigurineType::Major: return 7; break;
        case FigurineType::Captain: return 6; break;
        case FigurineType::Lieutenant: return 5; break;
        case FigurineType::Sergeant: return 4; break;
        case FigurineType::Miner: return 3; break;
        case FigurineType::Scout: return 2; break;
        case FigurineType::Spy: return 1; break;
        case FigurineType::Flag: return 0; break;
        case FigurineType::None: return 15; break;
        default: return -1;
    }
}

FigurineType Figurine::figurineTypeFromStrength(int strength) {
    switch (strength) {
        case 11: return FigurineType::Bomb; break;
        case 10: return FigurineType::Marshal; break;
        case 9: return FigurineType::General; break;
        case 8: return FigurineType::Colonel; break;
        case 7: return FigurineType::Major; break;
        case 6: return FigurineType::Captain; break;
        case 5: return FigurineType::Lieutenant; break;
        case 4: return FigurineType::Sergeant; break;
        case 3: return FigurineType::Miner; break;
        case 2: return FigurineType::Scout; break;
        case 1: return FigurineType::Spy; break;
        case 0: return FigurineType::Flag; break;
        case 15: return FigurineType::None; break;
        default: return FigurineType::None;
    }
}

QString Figurine::getImageSource(PlayerType pt, FigurineType ft, int strength) {
    QString resourceImage;
    QString prefix(":/Images/figurine");
    QString owner;
    QString type;
    if (pt == PlayerType::Human)
        owner = QString("p");
    else
        owner = QString("c");

    if (ft == FigurineType::Bomb)
        type = QString("B");
    else if (ft == FigurineType::Spy)
        type = QString("S");
    else if (ft == FigurineType::Flag)
        type = QString("F");
    else
        type = QString(QString::number(strength));

    QString postfix(".jpg");
    resourceImage.reserve(prefix.length() + type.length() + owner.length() +
                          postfix.length());
    resourceImage.append(prefix);
    resourceImage.append(type);
    resourceImage.append(owner);
    resourceImage.append(postfix);

    return resourceImage;
}

void Figurine::whereCanMove(int x, int y, Board* board) {
    Figurine* enemy     = nullptr;
    pState playingBoard = board->getPboard();

    if (this->getFigurineType() == FigurineType::Bomb ||
        this->getFigurineType() == FigurineType::Flag)
        return;

    else if (this->getFigurineType() == FigurineType::Scout) {
        int i = 1;
        while (x + i < 10 && (*playingBoard)[x + i][y]->canBeOccupied() &&
               !((*playingBoard)[x + i][y]->isForbidden())) {
            if (this->getOldI() != x + i || this->getOldJ() != y) {
                (*playingBoard)[x + i][y]->setAvailable(true);
                Field* f = (*playingBoard)[x + i][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
            i++;
        }

        if (x + i < 10 && (*playingBoard)[x + i][y]->isOccupied()) {
            enemy = (*playingBoard)[x + i][y]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x + i][y]->setAvailable(true);
                (*playingBoard)[x + i][y]->setEnemy(true);
                Field* f = (*playingBoard)[x + i][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        i = 1;
        while (x - i >= 0 && (*playingBoard)[x - i][y]->canBeOccupied() &&
               !((*playingBoard)[x - i][y]->isForbidden())) {
            if (this->getOldI() != x - i || this->getOldJ() != y) {
                (*playingBoard)[x - i][y]->setAvailable(true);
                Field* f = (*playingBoard)[x - i][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
            i++;
        }

        if (x - i >= 0 && (*playingBoard)[x - i][y]->isOccupied()) {
            enemy = (*playingBoard)[x - i][y]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x - i][y]->setAvailable(true);
                (*playingBoard)[x - i][y]->setEnemy(true);
                Field* f = (*playingBoard)[x - i][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        i = 1;
        while (y + i < 10 && (*playingBoard)[x][y + i]->canBeOccupied() &&
               !((*playingBoard)[x][y + i]->isForbidden())) {
            if (this->getOldI() != x || this->getOldJ() != y + i) {
                (*playingBoard)[x][y + i]->setAvailable(true);
                Field* f = (*playingBoard)[x][y + i];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
            i++;
        }

        if (y + i < 10 && (*playingBoard)[x][y + i]->isOccupied()) {
            enemy = (*playingBoard)[x][y + i]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x][y + i]->setAvailable(true);
                (*playingBoard)[x][y + i]->setEnemy(true);
                Field* f = (*playingBoard)[x][y + i];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        i = 1;
        while (y - i >= 0 && (*playingBoard)[x][y - i]->canBeOccupied() &&
               !((*playingBoard)[x][y - i]->isForbidden())) {
            if (this->getOldI() != x || this->getOldJ() != y - i) {
                (*playingBoard)[x][y - i]->setAvailable(true);
                Field* f = (*playingBoard)[x][y - i];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
            i++;
        }

        if (y - i >= 0 && (*playingBoard)[x][y - i]->isOccupied()) {
            enemy = (*playingBoard)[x][y - i]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x][y - i]->setAvailable(true);
                (*playingBoard)[x][y - i]->setEnemy(true);
                Field* f = (*playingBoard)[x][y - i];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }
    }

    else {
        if (x + 1 < 10 && (*playingBoard)[x + 1][y]->canBeOccupied() &&
            !((*playingBoard)[x + 1][y]->isForbidden())) {
            if (this->getOldI() != x + 1 || this->getOldJ() != y) {
                (*playingBoard)[x + 1][y]->setAvailable(true);
                Field* f = (*playingBoard)[x + 1][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        if (x + 1 < 10 && (*playingBoard)[x + 1][y]->isOccupied()) {
            enemy = (*playingBoard)[x + 1][y]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x + 1][y]->setAvailable(true);
                (*playingBoard)[x + 1][y]->setEnemy(true);
                Field* f = (*playingBoard)[x + 1][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        if (x - 1 >= 0 && (*playingBoard)[x - 1][y]->canBeOccupied() &&
            !((*playingBoard)[x - 1][y]->isForbidden())) {
            if (this->getOldI() != x - 1 || this->getOldJ() != y) {
                (*playingBoard)[x - 1][y]->setAvailable(true);
                Field* f = (*playingBoard)[x - 1][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        if (x - 1 >= 0 && (*playingBoard)[x - 1][y]->isOccupied()) {
            enemy = (*playingBoard)[x - 1][y]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x - 1][y]->setAvailable(true);
                (*playingBoard)[x - 1][y]->setEnemy(true);
                Field* f = (*playingBoard)[x - 1][y];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        if (y + 1 < 10 && (*playingBoard)[x][y + 1]->canBeOccupied() &&
            !((*playingBoard)[x][y + 1]->isForbidden())) {
            if (this->getOldI() != x || this->getOldJ() != y + 1) {
                (*playingBoard)[x][y + 1]->setAvailable(true);
                Field* f = (*playingBoard)[x][y + 1];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        if (y + 1 < 10 && (*playingBoard)[x][y + 1]->isOccupied()) {
            enemy = (*playingBoard)[x][y + 1]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x][y + 1]->setAvailable(true);
                (*playingBoard)[x][y + 1]->setEnemy(true);
                Field* f = (*playingBoard)[x][y + 1];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        if (y - 1 >= 0 && (*playingBoard)[x][y - 1]->canBeOccupied() &&
            !((*playingBoard)[x][y - 1]->isForbidden())) {
            if (this->getOldI() != x || this->getOldJ() != y - 1) {
                (*playingBoard)[x][y - 1]->setAvailable(true);
                Field* f = (*playingBoard)[x][y - 1];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }

        if (y - 1 >= 0 && (*playingBoard)[x][y - 1]->isOccupied()) {
            enemy = (*playingBoard)[x][y - 1]->getFigurine();

            if (this->getOwner() != enemy->getOwner()) {
                (*playingBoard)[x][y - 1]->setAvailable(true);
                (*playingBoard)[x][y - 1]->setEnemy(true);
                Field* f = (*playingBoard)[x][y - 1];
                f->update(f->getXPos(), f->getYPos(), f->getWidth(),
                          f->getHeight());
            }
        }
    }
}

/***GETTERS***/
int Figurine::getXPos() const { return m_xPos; }

int Figurine::getYPos() const { return m_yPos; }

int Figurine::getWidth() const { return m_width; }

int Figurine::getHeight() const { return m_height; }

int Figurine::getStrength() const { return m_strength; }

PlayerType Figurine::getOwner() const { return m_owner; }

FigurineType Figurine::getFigurineType() const { return m_figurineType; }

/***SETTERS***/
void Figurine::setXPos(int x) { m_xPos = x; }

void Figurine::setYPos(int y) { m_yPos = y; }

void Figurine::setWidth(int w) { m_width = w; }

void Figurine::setHeight(int h) { m_height = h; }

void Figurine::setStrength(int strength) { m_strength = strength; }

void Figurine::setOwner(PlayerType o) { m_owner = o; }

void Figurine::setFigurineType(FigurineType ft) { m_figurineType = ft; }

bool Figurine::getIsSelected() const { return m_isSelected; }

void Figurine::setSelected(bool value) { m_isSelected = value; }

void Figurine::setSize(int size) {
    this->setWidth(size);
    this->setHeight(size);
}

int Figurine::getOldI() const { return m_oldI; }

void Figurine::setOldI(int oldI) { m_oldI = oldI; }

int Figurine::getOldJ() const { return m_oldJ; }

void Figurine::setOldJ(int oldJ) { m_oldJ = oldJ; }

InBattle Figurine::getInBattle() const { return m_inBattle; }

void Figurine::setInBattle(InBattle inBattle) { m_inBattle = inBattle; }

bool Figurine::getCrossed() const { return m_crossed; }

void Figurine::setCrossed(bool crossed) { m_crossed = crossed; }
