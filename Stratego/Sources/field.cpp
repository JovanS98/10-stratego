#include "../Headers/field.h"

Field::~Field() {
    // delete m_figurine;
}

Field::Field(FieldType ft, int x, int y) {
    m_xPos             = x;
    m_yPos             = y;
    m_fieldType        = ft;
    m_figurine         = nullptr;
    m_width            = 120;
    m_height           = 120;
    m_available        = false;
    m_specialFieldFlag = 0;
    m_enemy            = false;

    m_fig = Figurine();
}

Field::Field(FieldType ft) {
    m_xPos             = 0;
    m_yPos             = 0;
    m_fieldType        = ft;
    m_figurine         = nullptr;
    m_width            = 0;
    m_height           = 0;
    m_available        = false;
    m_specialFieldFlag = 0;

    m_fig   = Figurine();
    m_enemy = false;
}

Field::Field(const Field &other) : QGraphicsItem() {
    setXPos(other.getXPos());
    setYPos(other.getYPos());
    setFieldType(other.getFieldType());
    setFigurine(other.getFigurine());
    setWidth(other.getWidth());
    setHeight(other.getHeight());
    setSpecialFieldFlag(other.getSpecialFieldFlag());

    m_fig = Figurine();
    setEnemy(other.getEnemy());
}

bool Field::canBeOccupied() {
    if (this->getFigurine() == nullptr &&
        this->getFieldType() != FieldType::Forbidden)
        return true;
    else
        return false;
}

bool Field::isOccupied() {
    if (this->getFigurine() != nullptr)
        return true;
    else
        return false;
}

bool Field::isForbidden() {
    if (this->getFieldType() == FieldType::Forbidden)
        return true;
    else
        return false;
}

QRectF Field::boundingRect() const {
    QPointF pointTopLeft = QPointF(getXPos(), getYPos());
    QPointF pointBottomRight =
        QPointF(getXPos() + getWidth(), getYPos() + getHeight());
    return QRectF(pointTopLeft, pointBottomRight);
}

void Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                  QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    int padding = 5;

    QRectF rect = boundingRect();

    painter->fillRect(rect, QColor::fromRgb(0, 0, 0));

    if (getSpecialFieldFlag() == 1) {
        QPixmap qpm = QPixmap(":/Images/lake00.jpg");
        painter->drawPixmap(getXPos() + padding, getYPos() + padding,
                            getWidth() - padding, getHeight() - padding, qpm);
    } else if (getSpecialFieldFlag() == 2) {
        QPixmap qpm = QPixmap(":/Images/lake01.jpg");
        painter->drawPixmap(getXPos(), getYPos() + padding,
                            getWidth() - padding, getHeight() - padding, qpm);
    } else if (getSpecialFieldFlag() == 3) {
        QPixmap qpm = QPixmap(":/Images/lake10.jpg");
        painter->drawPixmap(getXPos() + padding, getYPos(),
                            getWidth() - padding, getHeight() - padding, qpm);
    } else if (getSpecialFieldFlag() == 4) {
        QPixmap qpm = QPixmap(":/Images/lake11.jpg");
        painter->drawPixmap(getXPos(), getYPos(), getWidth() - padding,
                            getHeight() - padding, qpm);
    } else if (getSpecialFieldFlag() == 5) {
        QPixmap qpm = QPixmap(":/Images/2lake00.jpg");
        painter->drawPixmap(getXPos() + padding, getYPos() + padding,
                            getWidth() - padding, getHeight() - padding, qpm);
    } else if (getSpecialFieldFlag() == 6) {
        QPixmap qpm = QPixmap(":/Images/2lake01.jpg");
        painter->drawPixmap(getXPos(), getYPos() + padding,
                            getWidth() - padding, getHeight() - padding, qpm);
    } else if (getSpecialFieldFlag() == 7) {
        QPixmap qpm = QPixmap(":/Images/2lake10.jpg");
        painter->drawPixmap(getXPos() + padding, getYPos(),
                            getWidth() - padding, getHeight() - padding, qpm);
    } else if (getSpecialFieldFlag() == 8) {
        QPixmap qpm = QPixmap(":/Images/2lake11.jpg");
        painter->drawPixmap(getXPos(), getYPos(), getWidth() - padding,
                            getHeight() - padding, qpm);
    } else {
        if (getAvailable() == true) {
            if (!m_enemy) {
                float padding = getWidth() / 10;
                QPointF pointTopLeft =
                    QPointF(getXPos() + padding, getYPos() + padding);
                QPointF pointBottomRight =
                    QPointF(getXPos() + getWidth() - padding,
                            getYPos() + getHeight() - padding);
                QRectF innerRect(pointTopLeft, pointBottomRight);

                painter->fillRect(innerRect, QColor::fromRgb(102, 255, 102));
            }

            else {
                int width = 3;
                QPointF pointTopLeft =
                    QPointF(getXPos() + width, getYPos() + width);
                QPointF pointBottomRight =
                    QPointF(getXPos() + getWidth() - width,
                            getYPos() + getHeight() - width);
                QRectF innerRect(pointTopLeft, pointBottomRight);
                QPen pen;
                pen.setColor((Qt::red));
                pen.setWidth(width);
                painter->setPen(pen);

                painter->drawRect(innerRect);
            }
        } else {
            QPointF pointTopLeft =
                QPointF(getXPos() + padding, getYPos() + padding);
            QPointF pointBottomRight =
                QPointF(getXPos() + getWidth() - padding,
                        getYPos() + getHeight() - padding);
            QRectF innerRect(pointTopLeft, pointBottomRight);

            painter->fillRect(innerRect, QColor::fromRgb(228, 238, 255));
        }
    }
}

Field Field::operator=(Field other) { return Field(other); }

void Field::setFigurine(Figurine *figurine) { m_figurine = figurine; }

int Field::getWidth() const { return m_width; }

void Field::setWidth(int width) { m_width = width; }

int Field::getHeight() const { return m_height; }

void Field::setHeight(int height) { m_height = height; }

int Field::getXPos() const { return m_xPos; }

void Field::setXPos(int xPos) { m_xPos = xPos; }

int Field::getYPos() const { return m_yPos; }

void Field::setYPos(int yPos) { m_yPos = yPos; }

FieldType Field::getFieldType() const { return m_fieldType; }

void Field::setFieldType(const FieldType &fieldType) {
    m_fieldType = fieldType;
}

Figurine *Field::getFigurine() const { return m_figurine; }

bool Field::getAvailable() const { return m_available; }

void Field::setAvailable(bool available) { m_available = available; }

int Field::getSpecialFieldFlag() const { return m_specialFieldFlag; }

void Field::setSpecialFieldFlag(int value) { m_specialFieldFlag = value; }

bool Field::getEnemy() const { return m_enemy; }

void Field::setEnemy(bool enemy) { m_enemy = enemy; }

Figurine &Field::getFig() { return m_fig; }

void Field::setFig(Figurine fig) { m_fig = fig; }
