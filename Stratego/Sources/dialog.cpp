#include "../Headers/dialog.h"
#include "ui_dialog.h"

#include <QTime>

Dialog::Dialog(QWidget *parent) : QDialog(parent), ui(new Ui::Dialog) {
    ui->setupUi(this);
    mainWindow = nullptr;
    initializeMusic();
    connect(ui->pbOk, &QPushButton::pressed, this, &Dialog::on_pbOkPressed);
    ui->pbOk->setStyleSheet(
        "QPushButton "
        "{"
        "color: rgb(234, 211, 156);"
        "font: 12pt \"Ubuntu\"; "
        "background-color: rgb(102, 34, 37);"
        "border: 2px solid rgb(234, 211, 156);"
        "border-radius: 15px;border-style: outset;padding: 5px;"
        "} "
        "QPushButton:hover {background-color: rgb(157, 51, 31);"
        "border: 3px solid rgb(188, 109, 79);"
        "}");
}

Dialog::~Dialog() { delete ui; }

void Dialog::on_pbOkPressed() { close(); }

void delay(int n) {
    QTime dieTime = QTime::currentTime().addSecs(n);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void Dialog::showBattle(Figurine *figLeft, Figurine *figRight) {
    Dialog battleDialog;

    battleDialog.playBattleSound(figLeft, figRight);

    PlayerType pt   = figLeft->getOwner();
    FigurineType ft = figLeft->getFigurineType();
    int strength    = figLeft->getStrength();

    QPixmap pix(figLeft->getImageSource(pt, ft, strength));
    int w = battleDialog.ui->lblFigLeft->width();
    int h = battleDialog.ui->lblFigLeft->height();
    battleDialog.ui->lblFigLeft->setPixmap(
        pix.scaled(w, h, Qt::KeepAspectRatio));

    pt       = figRight->getOwner();
    ft       = figRight->getFigurineType();
    strength = figRight->getStrength();

    QPixmap pix1(figLeft->getImageSource(pt, ft, strength));
    battleDialog.ui->lblFigRight->setPixmap(
        pix1.scaled(w, h, Qt::KeepAspectRatio));

    QString cross = ":/Images/cross.png";
    QPixmap pix2(cross);

    if (*figLeft > *figRight)
        battleDialog.ui->lblFigRight2->setPixmap(
            pix2.scaled(w, h, Qt::KeepAspectRatio));

    else if (*figLeft == *figRight) {
        battleDialog.ui->lblFigRight2->setPixmap(
            pix2.scaled(w, h, Qt::KeepAspectRatio));
        battleDialog.ui->lblFigLeft2->setPixmap(
            pix2.scaled(w, h, Qt::KeepAspectRatio));
    }

    else
        battleDialog.ui->lblFigLeft2->setPixmap(
            pix2.scaled(w, h, Qt::KeepAspectRatio));

    battleDialog.move(1300, 350);

    battleDialog.exec();
}

void Dialog::playBattleSound(Figurine *figLeft, Figurine *figRight) {
    if (figRight->getFigurineType() == FigurineType::Bomb &&
        figLeft->getFigurineType() == FigurineType::Miner) {
        deactivateSound->play();
    }

    else if (figRight->getFigurineType() == FigurineType::Flag) {
        if (figRight->getOwner() == PlayerType::Computer)
            victorySound->play();
        else
            defeatSound->play();
    }

    else if (figRight->getFigurineType() == FigurineType::Bomb) {
        bombSound->play();
    } else {
        swordSound->play();
    }
}

void Dialog::initializeMusic() {
    bombSound = new QMediaPlayer(this);
    bombSound->setMedia(QUrl("qrc:/sounds/Sounds/bomb.wav"));
    swordSound = new QMediaPlayer(this);
    swordSound->setMedia(QUrl("qrc:/sounds/Sounds/sword.wav"));

    victorySound = new QMediaPlayer(this);
    victorySound->setMedia(QUrl("qrc:/sounds/Sounds/victory.mp3"));

    defeatSound = new QMediaPlayer(this);
    defeatSound->setMedia(QUrl("qrc:/sounds/Sounds/defeat.wav"));

    deactivateSound = new QMediaPlayer(this);
    deactivateSound->setMedia(QUrl("qrc:/sounds/Sounds/deactivate.flac"));
}
