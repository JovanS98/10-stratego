#-------------------------------------------------
#
# Project created by QtCreator 2020-11-20T11:57:14
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Stratego
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        Sources/main.cpp \
        Sources/mainwindow.cpp \
    Sources/figurine.cpp \
    Sources/player.cpp \
    Sources/field.cpp \
    Sources/tablesetupscene.cpp \
    Sources/game.cpp \
    Sources/gamescene.cpp \
    Sources/board.cpp \
    Sources/dialog.cpp \
    Sources/musicthread.cpp \
    Sources/tablefigurinescene.cpp

HEADERS += \
        Headers/mainwindow.h \
    Headers/figurine.h \
    Headers/figurinetype.h \
    Headers/player.h \
    Headers/field.h \
    Headers/enums.h \
    Headers/tablesetupscene.h \
    Headers/game.h \
    Headers/gamescene.h \
    Headers/board.h \
    Headers/dialog.h \
    Headers/musicthread.h \
    Headers/tablefigurinescene.h

FORMS += \
        Forms/mainwindow.ui \
    Forms/dialog.ui

RESOURCES += \
    Resources/icons.qrc \
    Resources/sounds.qrc \
    Resources/images.qrc
